//
//  NSString+Date.h
//  Maggie
//
//  Created by Samreen on 01/05/2017.
//  Copyright © 2017 Hannan Khan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Date)
-(NSString *) convertDateINtoLocalTimezone:(NSString *)dateStr;
@end
