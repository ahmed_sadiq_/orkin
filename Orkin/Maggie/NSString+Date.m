//
//  NSString+Date.m
//  Maggie
//
//  Created by Samreen on 01/05/2017.
//  Copyright © 2017 Hannan Khan. All rights reserved.
//

#import "NSString+Date.h"

@implementation NSString (Date)
-(NSString *) convertDateINtoLocalTimezone:(NSString *)dateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    NSDate *date = [dateFormatter dateFromString:dateStr]; // create date from string
    
    // change to a readable time format and change to local time zone
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone localTimeZone]];
    NSString *timestamp = [dateFormatter stringFromDate:date];
    return timestamp;
}
@end
