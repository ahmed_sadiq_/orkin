//
//  LocationManager.h
//  Ajo
//
//  Created by Samreen Noor on 14/10/2016.
//  Copyright © 2016 TxLabz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "LocationManager.h"
typedef void (^handlerSuccess)(CLLocationCoordinate2D location);
typedef void (^handlerError)(NSError *error);


typedef void (^serviceSuccess)(id data);
typedef void (^serviceFailure)(NSError *error);


@interface LocationManager : CLLocationManager
{
    handlerSuccess _success;
    handlerError _failed;
}
@property (nonatomic) float currentLat;
@property (nonatomic) float currentLong;
@property (nonatomic) NSString *locality;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *address;




+ (LocationManager *) sharedManager;

- (void) getUserCurrentLocationWithCompletionBlock:(handlerSuccess)completed
                                        errorBlock:(handlerError)errored;


+(void)getDistanceWithOrigin:(CLLocationCoordinate2D )origin
              andDestination:(CLLocationCoordinate2D )destination
                  andSuccess:(serviceSuccess)success
                  andFailure:(serviceFailure)failure;


@end
